module top(

xin,
xout,
rst_n,
sw,
led,
uart_rx,
uart_tx,
spi_sclk,
spi_csn,
spi_mosi,
spi_miso,
scl,
sda,
analog_cmp_n,
analog_cmp_p,
analog_out,
audio_pwm_out,
xclk,
clk_pll,
lock_pll,
  
casn,
rasn,
wen,
addr,
ba,
cke,
csn,
dqs_0,
dq_0,
ddrclk,
ldm

);
  input			xin;
  output		xout;

  input         rst_n;
  input  [3:0]  sw;
  output reg [3:0]  led=0;

  input         uart_rx;
  output        uart_tx;

  output        spi_sclk;
  output        spi_csn;
  output        spi_mosi;
  input         spi_miso;

  inout         scl;
  inout         sda;

  input			analog_cmp_n;
  input			analog_cmp_p;
  output		analog_out;

  output		audio_pwm_out;
  output		xclk;
  output        clk_pll;
  output        lock_pll;
  
  //ddr
	output wire casn;
	output wire rasn;
	output wire wen;
	output wire [12:0] addr;
	output wire [1:0] ba;
	output wire [0:0] cke;
	output wire [0:0] csn;
	inout wire dqs_0;
	inout wire [7:0] dq_0;
	output wire [0:0] ddrclk;
	output wire ldm;
	
     wire casn_din;
     wire datatri_0;
     wire dqso_0;
     wire dqstri_0;
     wire freeze;
     wire rasn_din;
     wire read_0;
     wire readclksel0_0;
     wire readclksel1_0;
     wire reset;
     wire sclk;
     wire uddcntln;
     wire wen_din;
     wire [12:0] addr_din;
     wire [1:0] ba_din;
     wire [0:0] cke_din;
     wire [0:0] csn_din;
     wire [15:0] dataout0;
	
     wire burstdet0;
     wire datavalid_0;
     wire lock;
     wire [15:0] datain0;
 
  
  //end ddr
  wire reset;
GSR GSR_INST (.GSR (GSR));

Clocks clocks(
	.xin(xin),
	.xout(xout),
	.clock_x24(xclk)
);
  reg [31:0]                  S0_ADR_I=0;

  reg  [15:0]                 S0_DAT_I=0;
  reg  [1:0]                  S0_SEL_I=0;
  wire [15:0]                 S0_DAT_O;

  reg                         S0_WE_I=0;
  reg [2:0]                   S0_CTI_I=0;
  reg [1:0]                   S0_BTE_I=0;
  reg                         S0_LOCK_I=0;
  reg                         S0_CYC_I=0;
  reg                         S0_STB_I=0;
 
  wire                        S0_ACK_O;
  wire                        S0_ERR_O;
  wire                        S0_RTY_O;
  wire                        S0_INT_O;

lpddr u1_lpddr(
// Inputs
   .rst_n   (~reset   ),
   .clk     (clk1     ),

   .S0_ADR_I (S0_ADR_I ),
   .S0_SEL_I (S0_SEL_I ),
   .S0_DAT_I (S0_DAT_I ),
   .S0_WE_I  (S0_WE_I  ),
 //  .S0_CTI_I (S0_CTI_I ),
   .S0_BTE_I (S0_BTE_I ),
//   .S0_LOCK_I(S0_LOCK_I),
   .S0_CYC_I (S0_CYC_I ),
   .S0_STB_I (S0_STB_I ),
   .CLK0_I   (clk1   ),
   .RST0_I   (reset   ),

   .S0_DAT_O(S0_DAT_O),
   .S0_ACK_O(S0_ACK_O),
   //.S0_ERR_O(S0_ERR_O),
   //.S0_RTY_O(S0_RTY_O),
   //.S0_INT_O(S0_INT_O),

  
// Bi_dir
   .em_ddr_dqs(dqs_0),
   .em_ddr_data(dq_0),

// Outputs


   .sclk(sclk),
   .em_ddr_cs_n(csn),
   .em_ddr_ras_n(rasn),
   .em_ddr_cas_n(casn),
   .em_ddr_we_n(wen),
   .em_ddr_ba(ba),
   .em_ddr_addr(addr),
   .em_ddr_cke(cke),
   .em_ddr_clk(ddrclk),
   .em_ddr_dm(ldm)
 );

reg [23:0] ctr=0;
always @ (posedge xclk) begin
	ctr<=ctr+1;
	led[3:1]<=sw[3:1];
end

parameter deflt=	2'bxxxx;
parameter idle =	2'b0000;
parameter read =	2'b0010;
parameter readack =	2'b0011;
parameter write=	2'b0100;
parameter writeack=	2'b0101;
parameter compare=	2'b0110;

reg [3:0] next_state;
reg dout;
reg  [15:0] datawrite=15'h5555;
reg  [15:0] dataread=0;

always @(posedge clk1 or posedge reset)
	if (reset) begin
		next_state <= idle;
	end
	else begin
		case(next_state)
			idle: begin
				next_state <= write;	
			end
			write: begin
				S0_ADR_I<=S0_ADR_I+2;
				S0_DAT_I<=datawrite;
				S0_SEL_I<=2'b11;
				S0_CYC_I<=1'b1;
				S0_STB_I<=1'b1;
				S0_WE_I<=1'b1;
				next_state <= writeack;
			end
			writeack: begin
				if(S0_ACK_O) begin
					S0_CYC_I<=1'b0;
					S0_STB_I<=1'b0;
					S0_WE_I<=1'b0;
				
					next_state <= read;
				end else 
					next_state <= writeack;
			end
			read: begin
				
				next_state <= readack;
			end	
			readack: begin
				if(S0_ACK_O) begin
					
					S0_CYC_I<=1'b0;
					S0_STB_I<=1'b0;
					S0_WE_I<=1'b0;
					dataread<=S0_DAT_O;
					next_state <= read;
				end else 
					next_state <= readack;
			end	
			compare: begin
				led[0:0]<=datawrite==dataread?1'b0:1'b1;
				next_state <= idle;
			end	
			default: begin
				next_state <= deflt;
			end
		endcase
	end
	

wire clk /* synthesis syn_keep = 1 */;
wire clk1;
defparam OSCH_inst.NOM_FREQ = "133.00"; 
OSCH OSCH_inst (.OSC(clk), .SEDSTDBY(), .STDBY(1'b0)); 

ddrpll test(.CLKI(clk),.CLKOP(clk1));

//---------------------------------------------------------------------
// modules



endmodule