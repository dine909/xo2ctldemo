module Clocks
(
	xin,xout,
	clock_x24
);

input	xin;
output	xout,clock_x24;

assign xout=~xin;
assign clock_x24=xin;



endmodule
