// ===========================================================================
// Filename: lpddr_ios.v  
// Copyright 2009 (c) Lattice Semiconductor Corporation. All rights reserved.
// ===========================================================================
`timescale 1ns/100ps
module lpddr_ios (
// Inputs
   rst_n,				// System reset
   clk,				
   write_data,			
   rst_acth,		      // Reset active high
   write_en,		
   write_dqm,				
   pio_read,			
   dqs_out,			
   tri_en,			
   init_done,
   BL4,
   BL8,
   BL16,
   specl,

`ifdef DATA_SIZE_16
   readclk0,
   readclk1,
`endif

`ifdef DATA_SIZE_8
   readclk0,
`endif

   dqsdel,   

   ddr_cs_n,
   ddr_ras_n,
   ddr_cas_n,
   ddr_we_n,
   ddr_ba,
   ddr_addr,
   ddr_cke,

// Bi-dir 
   em_ddr_data,				
   em_ddr_dqs,				

// Output 
   em_ddr_clk,				
   em_ddr_dm,			

   read_data,
   burstbet,
   dqsi,				

   em_ddr_cs_n,
   em_ddr_ras_n,
   em_ddr_cas_n,
   em_ddr_we_n,
   em_ddr_ba,
   em_ddr_addr,
   em_ddr_cke,
	
   data_valid				
);
`ifdef CON1
 `include "../source/top/xo2/config1/lpddr_params.v"
`endif
`ifdef CON2
 `include "../source/top/xo2/config2/lpddr_params.v"
`endif
`ifdef CON3
 `include "../source/top/xo2/config3/lpddr_params.v"
`endif
`ifdef CON4
 `include "../source/top/xo2/config4/lpddr_params.v"
`endif
`ifdef CON5
 `include "../source/top/xo2/config5/lpddr_params.v"
`endif
`ifdef CON6
 `include "../source/top/xo2/config6/lpddr_params.v"
`endif
`ifdef CON7
 `include "../source/top/xo2/config7/lpddr_params.v"
`endif
`ifdef CON8
 `include "../source/top/xo2/config8/lpddr_params.v"
`endif
`ifdef CON9
 `include "../source/top/xo2/config9/lpddr_params.v"
`endif

`ifdef CON10
 `include "../source/top/xo2/config10/lpddr_params.v"
`endif

`ifdef CON11
 `include "../source/top/xo2/config11/lpddr_params.v"
`endif

`ifdef CON12
 `include "../source/top/xo2/config12/lpddr_params.v"
//`else
// `include "lpddr_params.v"
`endif

// =======================================
// Define all inputs / outputs
// =======================================

input                          clk;
input					 write_en;
input  [`USER_DM-1:0]          write_dqm;
input                          pio_read;
input  [`DQS_WIDTH-1:0]        dqs_out;
input					 tri_en;
input  [`DQS_WIDTH-1:0]	       dqsdel;
input  [`DSIZE-1:0]            write_data;
input                          rst_n;
input					 rst_acth;

input                          init_done;
input                          BL4;
input                          BL8;
input                          BL16;
input                          specl;

`ifdef DATA_SIZE_16
  input [1:0]                  readclk0;
  input [1:0]                  readclk1;
`endif

`ifdef DATA_SIZE_8
  input [1:0]                  readclk0;
`endif

input                          ddr_cs_n;
input                          ddr_ras_n;
input                          ddr_cas_n;
input                          ddr_we_n;
input [1:0]                    ddr_ba;
input [`ROW_WIDTH-1:0]         ddr_addr;

input                          ddr_cke;

inout  [`DATA_WIDTH-1:0]       em_ddr_data;
inout  [`DQS_WIDTH-1:0]        em_ddr_dqs;
	
output [`DQS_WIDTH-1:0]        em_ddr_dm;
output [`CLKO_WIDTH-1:0]       em_ddr_clk;
output [`DSIZE-1:0]            read_data;    
output [`DQS_WIDTH-1:0]        data_valid;
output [`DQS_WIDTH-1:0]        dqsi;
output [`DQS_WIDTH-1:0]        burstbet;

output                         em_ddr_cs_n;
output                         em_ddr_ras_n;
output                         em_ddr_cas_n;
output                         em_ddr_we_n;

output [1:0]                   em_ddr_ba;
output [`ROW_WIDTH-1:0]        em_ddr_addr;

output                         em_ddr_cke;

// ========================================
// Regs & Wires
// ========================================

wire [`CLKO_WIDTH-1:0]         em_ddr_clk;
wire                           clk;
wire [`DSIZE-1:0]              read_data;

wire [`USER_DM-1:0]            write_dqm;
wire [`DSIZE-1:0]              write_data;

wire [`DQS_WIDTH-1:0]          ddrclkpol;
wire [`DQS_WIDTH-1:0]          dqsi;

wire [`DQS_WIDTH-1:0]          dqs_out;
wire                           write_en;
wire [`DQS_WIDTH-1:0]          dqsdel;
wire [`DQS_WIDTH-1:0]          data_valid;

wire [`DQS_WIDTH-1:0]          dqsw90;
wire [`DQS_WIDTH-1:0]          dqsr90;

`ifdef CLK_OFF
    reg                        mem_clk;
    reg                        ddr_cke_d0, ddr_cke_d1;
`endif

wire                           mem_clk_w /* synthesis syn_keep = 1 */;

`ifdef DATA_SIZE_16
  wire [1:0]                   readclk0;
  wire [1:0]                   readclk1;
`endif

`ifdef DATA_SIZE_8
  wire [1:0]                   readclk0;
`endif

wire [`DQS_WIDTH-1:0]          burstbet;

reg                            em_ddr_cs_n;
reg                            em_ddr_ras_n;
reg                            em_ddr_cas_n;
reg                            em_ddr_we_n;

reg [1:0]                      em_ddr_ba;
reg [`ROW_WIDTH-1:0]           em_ddr_addr;

reg                            em_ddr_cke;

always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
       em_ddr_ras_n            <=  1'b1;
       em_ddr_cas_n            <=  1'b1;
       em_ddr_we_n             <=  1'b1;
       em_ddr_ba               <=  'h0;
       em_ddr_addr             <=  'h0;
       em_ddr_cs_n             <=  1'b0;   //{`CS_WIDTH{1'b1}};
`ifdef CLK_OFF
`else
       em_ddr_cke              <=  'h0;  
`endif
    end
    else begin
       em_ddr_ras_n            <= ddr_ras_n;
       em_ddr_cas_n            <= ddr_cas_n;
       em_ddr_we_n             <= ddr_we_n;
       em_ddr_ba               <= ddr_ba;
       em_ddr_addr             <= ddr_addr;
       em_ddr_cs_n             <= ddr_cs_n;
`ifdef CLK_OFF
`else
       em_ddr_cke              <= ddr_cke;
`endif
    end
end

`ifdef CLK_OFF
always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) 
       mem_clk              <=  1'b0;  
    else if (specl && !ddr_cke)
       mem_clk              <=  1'b1;  
    else
       mem_clk              <=  1'b0;  
end   
  
always @ (posedge clk or negedge rst_n) begin
    if (!rst_n) begin
       ddr_cke_d0           <= 1'b0;
       ddr_cke_d1           <= 1'b0;
       em_ddr_cke              <=  'h0;  

    end
    else begin
       ddr_cke_d0           <= ddr_cke; //em_ddr_cke
       ddr_cke_d1           <= ddr_cke_d0;
       if (!ddr_cke && specl)
          em_ddr_cke              <=  'h0;  
       else if (ddr_cke_d0 && specl)
          em_ddr_cke              <=  'h1;  
       else if (!specl)
          em_ddr_cke              <=  ddr_cke;  

    end
end  
`endif
// ======================================
// Instintiate DDR Data/DQs and DM
// ======================================

data_io U1_data_io (
  // Inputs 
  .clk                    (clk),
  .rst_acth               (rst_acth),
  .rst_n                  (rst_n),
  .ddrclkpol              (ddrclkpol),
  .write_data             (write_data),
  .write_en               ({`DQS_WIDTH{write_en}}),
 
  .dqsr90                 (dqsr90),
  .dqsw90                 (dqsw90),
 
  // Outputs
  .read_data              (read_data),
  
  // bidir
  .em_ddr_data            (em_ddr_data)
);

dm_io U1_dm_io (
  // Inputs
  .clk                    (clk),
  .rst_acth               (rst_acth),
  .rst_n                  (rst_n),
  .write_dqm              (write_dqm),
  .dqsw90                 (dqsw90),
  .write_en               ({`DQS_WIDTH{write_en}}),

  // Outputs  
  .em_ddr_dm              (em_ddr_dm)
);

dqs_io U1_dqs_io (
  // Inputs
  .rst_acth               (rst_acth),
  .rst_n                  (rst_n),
  .clk                    (clk), 
  .pio_read               (pio_read), 
  .tri_en                 (tri_en), 
  .dqsdel                 (dqsdel), 
  .dqs_out                (dqs_out), 
  .init_done              (init_done),	
  .BL4                    (BL4),
  .BL8                    (BL8),
  .BL16                   (BL16),

`ifdef DATA_SIZE_16
  .readclk0               (readclk0),
  .readclk1               (readclk1),
`endif
	
`ifdef DATA_SIZE_8
  .readclk0               (readclk0),
`endif

  // Outputs
  .data_valid             (data_valid),
  .ddrclkpol              (ddrclkpol),  
  .dqsi                   (dqsi), 
  .dqsw90                 (dqsw90),
  .dqsr90                 (dqsr90),
  .burstbet               (burstbet),

  // bidir
  .em_ddr_dqs             (em_ddr_dqs)
);

// ==========================================
// Generate the clocks for the memory device
// ==========================================

`ifdef CLK_OFF
   assign mem_clk_w = (specl && !ddr_cke_d1 && !ddr_cke) ? mem_clk : clk;

`else
   assign mem_clk_w = clk;
`endif

ODDRXE U1_ODDRXE(
.D0(1'b0), 
.D1(1'b1), 
.RST(rst_acth), 

//.RST(1'b0), 
//.SCLK(clk), 
.SCLK(mem_clk_w), 

// output
.Q(em_ddr_clk ));



endmodule
