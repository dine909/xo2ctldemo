`timescale 1ns/1ps

module lpddr_tb;
`define WISHBONE
`ifdef CON1
 `include "../source/top/xo2/config1/lpddr_params.v"
`endif
`ifdef CON2
 `include "../source/top/xo2/config2/lpddr_params.v"
`endif
`ifdef CON3
 `include "../source/top/xo2/config3/lpddr_params.v"
`endif
`ifdef CON4
 `include "../source/top/xo2/config4/lpddr_params.v"
`endif
`ifdef CON5
 `include "../source/top/xo2/config5/lpddr_params.v"
`endif
`ifdef CON6
 `include "../source/top/xo2/config6/lpddr_params.v"
`endif
`ifdef CON7
 `include "../source/top/xo2/config7/lpddr_params.v"
`endif
`ifdef CON8
 `include "../source/top/xo2/config8/lpddr_params.v"
`endif
`ifdef CON9
 `include "../source/top/xo2/config9/lpddr_params.v"
`endif
`ifdef CON10
 `include "../source/top/xo2/config10/lpddr_params.v"
`endif
`ifdef CON11
 `include "../source/top/xo2/config11/lpddr_params.v"
`endif

`ifdef CON12
 `include "../source/top/xo2/config12/lpddr_params.v"
`endif

`ifdef CON13
 `include "../source/top/xo2/config13/lpddr_params.v"
`endif

`ifdef CON14
 `include "../source/top/xo2/config14/lpddr_params.v"
`endif


/////////////////////////////////////////////////////
// Internal nets and registers
/////////////////////////////////////////////////////

reg         clk_i;                                      
reg         rst_i;  
reg         clk_in;                                      
reg         rst_n;  
                                  


wire   [`DATA_WIDTH-1:0] em_ddr_data;        
wire   [`DQS_WIDTH-1:0]  em_ddr_dqs;       
wire   [`DQS_WIDTH-1:0]  em_ddr_dm;      


wire        em_ddr_clk;
wire        em_ddr_cke;
wire        em_ddr_ras_n;
wire        em_ddr_cas_n;
wire        em_ddr_we_n;
wire        em_ddr_cs_n;
wire [1:0]  em_ddr_ba;
wire [`ROW_WIDTH -1:0] em_ddr_addr;

`ifdef DATA_SIZE_16
  reg  [31:0] DDR_ADR_I;
  reg  [31:0] DDR_DAT_I;
  wire [31:0] DDR_DAT_O;
  reg  [3:0]  DDR_SEL_I;

  reg  [31:0] CMD_ADR_I;
  reg  [31:0] CMD_DAT_I;
  reg  [3:0]  CMD_SEL_I;

  wire [31:0] CMD_DAT_O;

`endif

`ifdef DATA_SIZE_8
  reg  [31:0] DDR_ADR_I;
  reg  [15:0] DDR_DAT_I;
  wire [15:0] DDR_DAT_O;
  reg  [1:0]  DDR_SEL_I;

  reg  [31:0] CMD_ADR_I;
  reg  [15:0] CMD_DAT_I;
  reg  [1:0]  CMD_SEL_I;
  wire [15:0] CMD_DAT_O;

`endif


reg         DDR_WE_I;
wire        DDR_ACK_O;
wire        DDR_ERR_O;
wire        DDR_RTY_O;
reg  [2:0]  DDR_CTI_I;
reg  [1:0]  DDR_BTE_I;
reg         DDR_LOCK_I;
reg         DDR_CYC_I;
reg         DDR_STB_I;
integer     err_cnt;
reg         DDR_ACK_O_d0, DDR_ACK_O_d1, DDR_ACK_O_d2, DDR_ACK_O_d3;
reg         DDR_ACK_O_d4, DDR_ACK_O_d5, DDR_ACK_O_d6, DDR_ACK_O_d7;
reg  [15:0] conf;

reg         CMD_WE_I;
reg  [2:0]  CMD_CTI_I;
reg  [1:0]  CMD_BTE_I;
reg         CMD_LOCK_I;
reg         CMD_CYC_I;
reg         CMD_STB_I;

reg         BL2, BL4, BL8, BL16;

wire        CMD_ACK_O;
wire        CMD_ERR_O;
wire        CMD_RTY_O;
reg         CMD_ACK_O_d;
wire        DDR_STB_I_in;

assign DDR_STB_I_in = (BL2)? DDR_STB_I && !DDR_ACK_O_d0:
 (BL4 ? DDR_STB_I && !(DDR_ACK_O_d1 && DDR_ACK_O_d0): (BL8 ? DDR_STB_I && !(DDR_ACK_O_d3 && DDR_ACK_O_d2) : DDR_STB_I && !DDR_ACK_O_d7  ));

/////////////////////////////////////////////////////
// Instantiations
/////////////////////////////////////////////////////

lpddr_top u1_lpddr_top(
// Inputs
   .clk_in             (clk_in),
   .rst_n              (rst_n),

   .CLK0_I              (clk_i),
   .RST0_I              (rst_i), 
`ifdef ONE_PORT
   .S0_ADR_I            (DDR_ADR_I),
   .S0_DAT_I            (DDR_DAT_I),
   .S0_DAT_O            (DDR_DAT_O),
   .S0_SEL_I            (DDR_SEL_I),
   .S0_WE_I             (DDR_WE_I ), 
   .S0_ACK_O            (DDR_ACK_O),
   .S0_ERR_O            (DDR_ERR_O),
   .S0_RTY_O            (DDR_RTY_O),
   .S0_CTI_I            (DDR_CTI_I),
   .S0_BTE_I            (DDR_BTE_I),
   .S0_LOCK_I           (DDR_LOCK_I),
   .S0_CYC_I            (DDR_CYC_I ),
   .S0_STB_I            ((DDR_WE_I)? DDR_STB_I && !DDR_ACK_O_d0:DDR_STB_I_in),
 `else 

   .S0_ADR_I            (DDR_ADR_I),
   .S0_DAT_I            (DDR_DAT_I),
   .S0_DAT_O            (DDR_DAT_O),
   .S0_SEL_I            (DDR_SEL_I),
   .S0_WE_I             (DDR_WE_I ), 
   .S0_ACK_O            (DDR_ACK_O),
   .S0_ERR_O            (DDR_ERR_O),
   .S0_RTY_O            (DDR_RTY_O),
   .S0_CTI_I            (DDR_CTI_I),
   .S0_BTE_I            (DDR_BTE_I),
   .S0_LOCK_I           (DDR_LOCK_I),
   .S0_CYC_I            (DDR_CYC_I ),
   .S0_STB_I            ((DDR_WE_I)? DDR_STB_I && !DDR_ACK_O_d0:DDR_STB_I_in),


   .CLK1_I              (clk_i),
   .RST1_I              (rst_i), 

   .S1_ADR_I            (CMD_ADR_I ),
   .S1_DAT_I            (CMD_DAT_I ),
   .S1_DAT_O            (CMD_DAT_O),
   .S1_SEL_I            (CMD_SEL_I ),
   .S1_WE_I             (CMD_WE_I ),
   .S1_ACK_O            (CMD_ACK_O),
   .S1_ERR_O            (CMD_ERR_O),
   .S1_RTY_O            (CMD_RTY_O),
   .S1_CTI_I            (CMD_CTI_I),
   .S1_BTE_I            (CMD_BTE_I),
   .S1_LOCK_I           (CMD_LOCK_I),
   .S1_CYC_I            (CMD_CYC_I ),
   .S1_STB_I            (CMD_STB_I && !CMD_ACK_O_d),
`endif
// Bi_dir
   .em_ddr_dqs         (em_ddr_dqs),
   .em_ddr_data        (em_ddr_data),

// Outputs

   .em_ddr_cs_n        (em_ddr_cs_n),
   .em_ddr_ras_n       (em_ddr_ras_n),
   .em_ddr_cas_n       (em_ddr_cas_n),
   .em_ddr_we_n        (em_ddr_we_n),
   .em_ddr_ba          (em_ddr_ba),
   .em_ddr_addr        (em_ddr_addr),
   .em_ddr_cke         (em_ddr_cke),
   .em_ddr_clk         (em_ddr_clk),
   .em_ddr_dm          (em_ddr_dm)
);

`ifdef DATA_SIZE_16
mobile_ddr_16   u1_mobile_ddr_16    (
   .ddr_dq             (em_ddr_data),
   .ddr_dqs            (em_ddr_dqs),
   .ddr_dqm            (em_ddr_dm),
   .ddr_ad             (em_ddr_addr),
   .ddr_ba             (em_ddr_ba),
   .ddr_ras_n          (em_ddr_ras_n),
   .ddr_cas_n          (em_ddr_cas_n), 
   .ddr_we_n           (em_ddr_we_n), 
   .ddr_cs_n           (em_ddr_cs_n),
   .ddr_clk            (em_ddr_clk),
   .ddr_clk_n          (~em_ddr_clk),
   .ddr_cke            (em_ddr_cke)
);
`else
mobile_ddr_8   u1_mobile_ddr_8    (
   .ddr_dq             (em_ddr_data),
   .ddr_dqs            (em_ddr_dqs),
   .ddr_dqm            (em_ddr_dm),
   .ddr_ad             (em_ddr_addr),
   .ddr_ba             (em_ddr_ba),
   .ddr_ras_n          (em_ddr_ras_n),
   .ddr_cas_n          (em_ddr_cas_n), 
   .ddr_we_n           (em_ddr_we_n), 
   .ddr_cs_n           (em_ddr_cs_n),
   .ddr_clk            (em_ddr_clk),
   .ddr_clk_n          (~em_ddr_clk),
   .ddr_cke            (em_ddr_cke)
);

`endif
parameter c = 5 ; 

initial begin
`ifdef ONE_PORT
 DDR_ADR_I  = 0;
 DDR_DAT_I  = 0;
 DDR_SEL_I  = 0;
 DDR_WE_I   = 0;
 DDR_CTI_I  = 0;
 DDR_BTE_I  = 0;
 DDR_LOCK_I = 0;
 DDR_CYC_I  = 0;
 DDR_STB_I  = 0;
`else

 DDR_ADR_I  = 0;
 DDR_DAT_I  = 0;
 DDR_SEL_I  = 0;
 DDR_WE_I   = 0;
 DDR_CTI_I  = 0;
 DDR_BTE_I  = 0;
 DDR_LOCK_I = 0;
 DDR_CYC_I  = 0;
 DDR_STB_I  = 0;

 CMD_ADR_I  = 0;
 CMD_DAT_I  = 0;
 CMD_SEL_I  = 0;
 CMD_WE_I   = 0;
 CMD_CTI_I  = 0;
 CMD_BTE_I  = 0;
 CMD_LOCK_I = 0;
 CMD_CYC_I  = 0;
 CMD_STB_I  = 0;
`endif
 conf       = `MRS_REG ;
 if (conf ==16'h0031)    BL2  = 1;
 else BL2  = 0;
 
 if (conf ==16'h0032)    BL4  = 1;
 else BL4  = 0;
 
 if (conf ==16'h0033)    BL8  = 1;
 else BL8  = 0;
 
 if (conf ==16'h0034)    BL16  = 1;
 else BL16  = 0;
 
 clk_i      = 0;
 rst_i      = 1;
 clk_in      = 0;
 rst_n      = 0;

 #(60) rst_i    = 0;
 #(60) rst_n    = 1;

end


always #(5) clk_i   = ~clk_i;
always #(c) clk_in   = ~clk_in;


always @(posedge rst_i or posedge clk_i) begin
   if(rst_i) CMD_ACK_O_d <= #1 1'b0;
   else      CMD_ACK_O_d <= #1 CMD_ACK_O;
end

always @(posedge rst_i or posedge clk_i) begin
   if(rst_i) begin
     DDR_ACK_O_d0 <= #1 1'b0;
     DDR_ACK_O_d1 <= #1 1'b0;
     DDR_ACK_O_d2 <= #1 1'b0;
     DDR_ACK_O_d3 <= #1 1'b0;
     DDR_ACK_O_d4 <= #1 1'b0;
     DDR_ACK_O_d5 <= #1 1'b0;
     DDR_ACK_O_d6 <= #1 1'b0;
     DDR_ACK_O_d7 <= #1 1'b0;
   end
   else begin
     DDR_ACK_O_d0 <= #1 DDR_ACK_O;
     DDR_ACK_O_d1 <= #1 DDR_ACK_O_d0;
     DDR_ACK_O_d2 <= #1 DDR_ACK_O_d1;
     DDR_ACK_O_d3 <= #1 DDR_ACK_O_d2;
     DDR_ACK_O_d4 <= #1 DDR_ACK_O_d3;
     DDR_ACK_O_d5 <= #1 DDR_ACK_O_d4;
     DDR_ACK_O_d6 <= #1 DDR_ACK_O_d5;
     DDR_ACK_O_d7 <= #1 DDR_ACK_O_d6;

   end
end

// -------------------------------------------------------

task wb_write;
input   [31:0]  addr;

`ifdef DATA_SIZE_16
  input   [31:0]  data;
`endif
`ifdef DATA_SIZE_8
  input   [15:0]  data;
`endif

integer i;

begin
   @(posedge clk_i);
   // assert cyc/stb signals
   #1;
   DDR_CYC_I  = 1'b1;
   DDR_STB_I  = 1'b1;
   DDR_ADR_I  = addr;
   DDR_DAT_I  = data;
   DDR_WE_I   = 1'b1;
   DDR_CTI_I  = 3'b000;

`ifdef DATA_SIZE_16
   DDR_SEL_I  = 4'b1111;
`endif
`ifdef DATA_SIZE_8
   DDR_SEL_I  = 2'b11;
`endif


   for (i=0;i<128;i=i+1) begin
      while(~DDR_ACK_O )      @(posedge clk_i);
      $display ("At time %t: wishbone write data %8x at address %8x", $time,DDR_DAT_I,DDR_ADR_I);
      DDR_ADR_I  = DDR_ADR_I+2;
      DDR_DAT_I  = DDR_DAT_I+1;


      @(posedge clk_i);
   end

   // negate wishbone signals
   #1;
   DDR_CYC_I  = 1'b0;
   DDR_STB_I  = 1'b0;
   DDR_ADR_I  = 0;
   DDR_DAT_I  = 0;
   DDR_WE_I   = 1'h0;
   DDR_CTI_I  = 3'b000;
end
endtask
// -------------------------------------------------------
task wb_read;
input    [31:0] addr;
integer i;

begin
   @(posedge clk_i);
   // assert cyc/stb signals
   #1;
   DDR_CYC_I  = 1'b1;
   DDR_STB_I  = 1'b1;
   DDR_ADR_I  = addr;
   DDR_WE_I   = 1'b0;
   DDR_CTI_I  = 3'b000;
`ifdef DATA_SIZE_16
   DDR_SEL_I  = 4'b1111;
`endif
`ifdef DATA_SIZE_8
   DDR_SEL_I  = 2'b11;
`endif


if (BL2) begin

   for (i=0;i<128;i=i+1) begin

      while(~DDR_ACK_O )      @(posedge clk_i);
`ifdef DATA_SIZE_16
   
	  if (BL2 && (DDR_DAT_O==(DDR_ADR_I-'h1f0)/2+32'h01020304))
`endif
`ifdef DATA_SIZE_8
    
	  if (BL2 && (DDR_DAT_O==(DDR_ADR_I-'h1f0)/2+16'h0102))
`endif
         $display ("At time %t: RIGHT!! at address %8x", $time,DDR_ADR_I);
      else if (BL2) begin
         $display ("At time %t: ERROR!!wishbone read data %8x at address %8x", $time,DDR_DAT_O,DDR_ADR_I);
         err_cnt = err_cnt + 1;
      end
      DDR_ADR_I  = DDR_ADR_I+2;

      @(posedge clk_i);
   end

   #1;
   DDR_CYC_I  = 1'b0;
   DDR_STB_I  = 1'b0;
   DDR_ADR_I  = 0;
   DDR_DAT_I  = 0;
   DDR_WE_I   = 1'h0;
   DDR_CTI_I  = 3'b000;

end // BL2
else if (BL4) begin
   for (i=0;i<64;i=i+1) begin
      while(~DDR_ACK_O )      @(posedge clk_i);
`ifdef DATA_SIZE_16
     
	  if  (DDR_DAT_O==(DDR_ADR_I-'h1f0)/2+32'h01020304)
`endif
`ifdef DATA_SIZE_8
    
	  if  (DDR_DAT_O==(DDR_ADR_I-'h1f0)/2+32'h0102)
`endif
         $display ("At time %t: RIGHT!! at address %8x", $time,DDR_ADR_I);
      else if (BL2) begin
         $display ("At time %t: ERROR!!wishbone read data %8x at address %8x", $time,DDR_DAT_O,DDR_ADR_I);
         err_cnt = err_cnt + 1;
      end
      DDR_ADR_I  = DDR_ADR_I+2;

      @(posedge clk_i);
   end

   #1;
   DDR_CYC_I  = 1'b0;
   DDR_STB_I  = 1'b0;
   DDR_ADR_I  = 0;
   DDR_DAT_I  = 0;
   DDR_WE_I   = 1'h0;
   DDR_CTI_I  = 3'b000;
end // BL4

else if (BL8) begin
   for (i=0;i<32;i=i+1) begin
      while(~DDR_ACK_O )      @(posedge clk_i);
`ifdef DATA_SIZE_16
     
	  if  (DDR_DAT_O==(DDR_ADR_I-'h1f0)/2+32'h01020304)
`endif
`ifdef DATA_SIZE_8
     
	  if  (DDR_DAT_O==(DDR_ADR_I-'h1f0)/2+32'h0102)
`endif

         $display ("At time %t: RIGHT!! at address %8x", $time,DDR_ADR_I);
      else if (BL2) begin
         $display ("At time %t: ERROR!!wishbone read data %8x at address %8x", $time,DDR_DAT_O,DDR_ADR_I);
         err_cnt = err_cnt + 1;
      end
      DDR_ADR_I  = DDR_ADR_I+2;

      @(posedge clk_i);
   end

   #1;
   DDR_CYC_I  = 1'b0;
   DDR_STB_I  = 1'b0;
   DDR_ADR_I  = 0;
   DDR_DAT_I  = 0;
   DDR_WE_I   = 1'h0;
   DDR_CTI_I  = 3'b000;

end // BL8
else if (BL16) begin
   for (i=0;i<16;i=i+1) begin
      while(~DDR_ACK_O )      @(posedge clk_i);
`ifdef DATA_SIZE_16
    
	  if  (DDR_DAT_O==(DDR_ADR_I-'h1f0)/2+32'h01020304)
`endif
`ifdef DATA_SIZE_8
    
	  if  (DDR_DAT_O==(DDR_ADR_I-'h1f0)/2+32'h0102)
`endif

         $display ("At time %t: RIGHT!! at address %8x", $time,DDR_ADR_I);
      else if (BL2) begin
         $display ("At time %t: ERROR!!wishbone read data %8x at address %8x", $time,DDR_DAT_O,DDR_ADR_I);
         err_cnt = err_cnt + 1;
      end
      DDR_ADR_I  = DDR_ADR_I+2;

      @(posedge clk_i);
   end

   #1;
   DDR_CYC_I  = 1'b0;
   DDR_STB_I  = 1'b0;
   DDR_ADR_I  = 0;
   DDR_DAT_I  = 0;
   DDR_WE_I   = 1'h0;
   DDR_CTI_I  = 3'b000;

end // BL16

end
endtask
// ------------------------------------------------------

`ifdef ONE_PORT

`else

task wb_pde;
integer i;
begin
   @(posedge clk_i);
   // assert cyc/stb signals
   #1;
   CMD_CYC_I  = 1'b1;
   CMD_STB_I  = 1'b1;
   CMD_ADR_I  = 32'h4;
   CMD_WE_I   = 1'b1;
   CMD_CTI_I  = 3'b000;
`ifdef DATA_SIZE_16
   CMD_SEL_I  = 4'b0001;
`endif
`ifdef DATA_SIZE_8
   CMD_SEL_I  = 2'b01;
`endif

   // wait for acknowledge from slave
   for (i=0;i<1;i=i+1) begin
      while(~CMD_ACK_O )      @(posedge clk_i);
      $display ("At time %t: wishbone sent PDE", $time);
      @(posedge clk_i);
   end

   // negate wishbone signals
   #1;
   CMD_CYC_I  = 1'b0;
   CMD_STB_I  = 1'b0;
   CMD_ADR_I  = 0;
   CMD_DAT_I  = 0;
   CMD_WE_I   = 1'h0;
   CMD_CTI_I  = 3'b000;
end
endtask

// -------------------------------------------------------

task wb_pdx;
integer i;
begin
   @(posedge clk_i);
   // assert cyc/stb signals
   #1;
   CMD_CYC_I  = 1'b1;
   CMD_STB_I  = 1'b1;
   CMD_ADR_I  = 32'h8;
   CMD_WE_I   = 1'b1;
   CMD_CTI_I  = 3'b000;
`ifdef DATA_SIZE_16
   CMD_SEL_I  = 4'b0001;
`endif
`ifdef DATA_SIZE_8
   CMD_SEL_I  = 2'b01;
`endif

   // wait for acknowledge from slave
   for (i=0;i<1;i=i+1) begin
      while(~CMD_ACK_O )      @(posedge clk_i);
      $display ("At time %t: wishbone sent PDX", $time);
      @(posedge clk_i);
   end

   // negate wishbone signals
   #1;
   CMD_CYC_I  = 1'b0;
   CMD_STB_I  = 1'b0;
   CMD_ADR_I  = 0;
   CMD_DAT_I  = 0;
   CMD_WE_I   = 1'h0;
   CMD_CTI_I  = 3'b000;
end
endtask

// -------------------------------------------------------

task wb_dpde;
integer i;
begin
   @(posedge clk_i);
   // assert cyc/stb signals
   #1;
   CMD_CYC_I  = 1'b1;
   CMD_STB_I  = 1'b1;
   CMD_ADR_I  = 32'h10;
   CMD_WE_I   = 1'b1;
   CMD_CTI_I  = 3'b000;
`ifdef DATA_SIZE_16
   CMD_SEL_I  = 4'b0001;
`endif
`ifdef DATA_SIZE_8
   CMD_SEL_I  = 2'b01;
`endif

   // wait for acknowledge from slave
   for (i=0;i<1;i=i+1) begin
      while(~CMD_ACK_O )      @(posedge clk_i);
      $display ("At time %t: wishbone sent DPDE", $time);
      @(posedge clk_i);
   end

   // negate wishbone signals
   #1;
   CMD_CYC_I  = 1'b0;
   CMD_STB_I  = 1'b0;
   CMD_ADR_I  = 0;
   CMD_DAT_I  = 0;
   CMD_WE_I   = 1'h0;
   CMD_CTI_I  = 3'b000;
end
endtask

// -------------------------------------------------------

task wb_dpdx;
integer i;
begin
   @(posedge clk_i);
   // assert cyc/stb signals
   #1;
   CMD_CYC_I  = 1'b1;
   CMD_STB_I  = 1'b1;
   CMD_ADR_I  = 32'h20;
   CMD_WE_I   = 1'b1;
   CMD_CTI_I  = 3'b000;
`ifdef DATA_SIZE_16
   CMD_SEL_I  = 4'b0001;
`endif
`ifdef DATA_SIZE_8
   CMD_SEL_I  = 2'b01;
`endif

   // wait for acknowledge from slave
   for (i=0;i<1;i=i+1) begin
      while(~CMD_ACK_O )      @(posedge clk_i);
      $display ("At time %t: wishbone sent DPDX", $time);
      @(posedge clk_i);
   end

   // negate wishbone signals
   #1;
   CMD_CYC_I  = 1'b0;
   CMD_STB_I  = 1'b0;
   CMD_ADR_I  = 0;
   CMD_DAT_I  = 0;
   CMD_WE_I   = 1'h0;
   CMD_CTI_I  = 3'b000;
end
endtask

// -------------------------------------------------------

task wb_sre;
integer i;
begin
   @(posedge clk_i);
   // assert cyc/stb signals
   #1;
   CMD_CYC_I  = 1'b1;
   CMD_STB_I  = 1'b1;
   CMD_ADR_I  = 32'h40;
   CMD_WE_I   = 1'b1;
   CMD_CTI_I  = 3'b000;
`ifdef DATA_SIZE_16
   CMD_SEL_I  = 4'b0001;
`endif
`ifdef DATA_SIZE_8
   CMD_SEL_I  = 2'b01;
`endif


   // wait for acknowledge from slave
   for (i=0;i<1;i=i+1) begin
      while(~CMD_ACK_O )      @(posedge clk_i);
      $display ("At time %t: wishbone sent SRE", $time);
      @(posedge clk_i);
   end

   // negate wishbone signals
   #1;
   CMD_CYC_I  = 1'b0;
   CMD_STB_I  = 1'b0;
   CMD_ADR_I  = 0;
   CMD_DAT_I  = 0;
   CMD_WE_I   = 1'h0;
   CMD_CTI_I  = 3'b000;
end
endtask

// -------------------------------------------------------

task wb_srx;
integer i;
begin
   @(posedge clk_i);
   // assert cyc/stb signals
   #1;
   CMD_CYC_I  = 1'b1;
   CMD_STB_I  = 1'b1;
   CMD_ADR_I  = 32'h80;
   CMD_WE_I   = 1'b1;
   CMD_CTI_I  = 3'b000;
`ifdef DATA_SIZE_16
   CMD_SEL_I  = 4'b0001;
`endif
`ifdef DATA_SIZE_8
   CMD_SEL_I  = 2'b01;
`endif


   // wait for acknowledge from slave
   for (i=0;i<1;i=i+1) begin
      while(~CMD_ACK_O )      @(posedge clk_i);
      $display ("At time %t: wishbone sent SRX", $time);
      @(posedge clk_i);
   end

   // negate wishbone signals
   #1;
   CMD_CYC_I  = 1'b0;
   CMD_STB_I  = 1'b0;
   CMD_ADR_I  = 0;
   CMD_DAT_I  = 0;
   CMD_WE_I   = 1'h0;
   CMD_CTI_I  = 3'b000;
end
endtask

// ------------------------------------------------------

task wb_mrs;
`ifdef DATA_SIZE_16
  input   [31:0]  data;
`endif

`ifdef DATA_SIZE_8
  input   [15:0]  data;
`endif

integer i;
begin
   @(posedge clk_i);
   // assert cyc/stb signals
   #1;
   CMD_CYC_I  = 1'b1;
   CMD_STB_I  = 1'b1;
   CMD_ADR_I  = 32'h1;
   CMD_DAT_I  = data;
   CMD_WE_I   = 1'b1;
   CMD_CTI_I  = 3'b000;
`ifdef DATA_SIZE_16
   CMD_SEL_I  = 4'b0001;
`endif
`ifdef DATA_SIZE_8
   CMD_SEL_I  = 2'b01;
`endif

   if (data[2:0] == 3'b010) begin
    BL2 = 0;
    BL4 = 1;
    BL8 = 0;
    BL16 = 0;
   end
   else if (data[2:0] == 3'b011) begin
    BL2 = 0;
    BL4 = 0;
    BL8 = 1;
    BL16 = 0;
   end
   else if (data[2:0] == 3'b100) begin
    BL2 = 0;
    BL4 = 0;
    BL8 = 0;
    BL16 = 1;
   end

   // wait for acknowledge from slave
   for (i=0;i<1;i=i+1) begin
      while(~CMD_ACK_O )      @(posedge clk_i);
      $display ("At time %t: wishbone sent MRS", $time);
      @(posedge clk_i);
   end

   // negate wishbone signals
   #1;
   CMD_CYC_I  = 1'b0;
   CMD_STB_I  = 1'b0;
   CMD_ADR_I  = 0;
   CMD_DAT_I  = 0;
   CMD_WE_I   = 1'h0;
   CMD_CTI_I  = 3'b000;
end
endtask

// ------------------------------------------------------

task wb_emrs;
`ifdef DATA_SIZE_16
  input   [31:0]  data;
`endif
`ifdef DATA_SIZE_8
  input   [15:0]  data;
`endif
integer i;
begin
   @(posedge clk_i);
   // assert cyc/stb signals
   #1;
   CMD_CYC_I  = 1'b1;
   CMD_STB_I  = 1'b1;
   CMD_ADR_I  = 32'h2;
   CMD_DAT_I  = data;
   CMD_WE_I   = 1'b1;
   CMD_CTI_I  = 3'b000;
`ifdef DATA_SIZE_16
   CMD_SEL_I  = 4'b0001;
`endif
`ifdef DATA_SIZE_8
   CMD_SEL_I  = 2'b01;
`endif

   // wait for acknowledge from slave
   for (i=0;i<1;i=i+1) begin
      while(~CMD_ACK_O )      @(posedge clk_i);
      $display ("At time %t: wishbone sent EMRS", $time);
      @(posedge clk_i);
   end

   // negate wishbone signals
   #1;
   CMD_CYC_I  = 1'b0;
   CMD_STB_I  = 1'b0;
   CMD_ADR_I  = 0;
   CMD_DAT_I  = 0;
   CMD_WE_I   = 1'h0;
   CMD_CTI_I  = 3'b000;
end
endtask

`endif
// ------------------------------------------------------

`ifdef ONE_PORT
  initial begin
  #1000;
  err_cnt=0;
  #10;
 
  #10;
  `ifdef DATA_SIZE_16
   
	 wb_write('h1f0,32'h01020304);
  `endif
  `ifdef DATA_SIZE_8
    
	 wb_write('h1f0,16'h0102);
  `endif

  #500;
  
  wb_read ('h1f0);
  #1000;
  if (err_cnt)
       $display ("TB INFO: The simulation is FAILED - errors are detected\n");
  else $display ("TB INFO: The simulation has PASSED \n");

  $finish;
  end
`else

initial begin
#1000;
err_cnt=0;
#10;

`ifdef DATA_SIZE_16
 
   wb_write('h1f0,32'h01020304);
`endif
`ifdef DATA_SIZE_8
 
   wb_write('h1f0,16'h0102);
`endif

#500;

wb_read ('h1f0);
#500;
wb_pde;
#50;
wb_pdx;
#50;
wb_pde;
#10;
wb_pdx;
#10;
#10;
#10;
wb_sre;
#10;
wb_srx;
#10;
`ifdef DATA_SIZE_16
   wb_mrs(32'h00000032); 
`endif
`ifdef DATA_SIZE_8
   wb_mrs(16'h0032); 
`endif

#10;
`ifdef DATA_SIZE_16
 
  wb_write('h1f0,32'h01020304);
`endif
`ifdef DATA_SIZE_8
 
  wb_write('h1f0,16'h0102);
`endif

#500;


wb_read ('h1f0);
#500;
`ifdef DATA_SIZE_16
  wb_mrs(32'h00000033); 
  #10;
 
  wb_write('h1f0,32'h01020304);
`endif
`ifdef DATA_SIZE_8
  wb_mrs(16'h0033); 
  #10;
 
  wb_write('h1f0,16'h0102);
`endif

#500;


wb_read ('h1f0);
#500;
`ifdef DATA_SIZE_16
  wb_mrs(32'h00000034); 
  #10;

  wb_write('h1f0,32'h01020304);
`endif
`ifdef DATA_SIZE_8
  wb_mrs(16'h0034); 
  #10;
 
  wb_write('h1f0,16'h0102);
`endif

#500;


wb_read ('h1f0);
#1000;


if (err_cnt)
     $display ("TB INFO: The simulation is FAILED - errors are detected\n");
else $display ("TB INFO: The simulation has PASSED \n");


$finish;
end

`endif

GSR GSR_INST (.GSR(rst_n)); 
PUR PUR_INST (.PUR(rst_n)); 


initial begin
   //$recordfile ("wishbone.trn");
   //$recordvars ();
end

endmodule

