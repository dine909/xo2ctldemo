//---LPDDR Verilog Parameter File---

`define LATTICE_FAMILY "XO2"

`define WISHBONE
`define TRAINING_ON
`define RE_TRAIN
`define ONE_PORT
`define BL4_FIXED
`define DATA_SIZE_8

`define ADDR_WIDTH 23
`define ROW_WIDTH   12

`define INT_BANK_4
`define BSIZE          2
`define BNK_WIDTH      2
`define CS_WIDTH       1
`define CKE_WIDTH      1
`define CLKO_WIDTH     1
`define CLKO_WIDTH_1

`define AR_CNT 8
`define AR_BURST_8

`ifdef AR_BURST_8
   `define AR_BURST_EN 4'b1000
`else
   `ifdef AR_BURST_7
      `define AR_BURST_EN 4'b0111
   `else
      `ifdef AR_BURST_6
         `define AR_BURST_EN 4'b0110
      `else
         `ifdef AR_BURST_5
            `define AR_BURST_EN 4'b0101
         `else
            `ifdef AR_BURST_4
               `define AR_BURST_EN 4'b0100
            `else
               `ifdef AR_BURST_3
                  `define AR_BURST_EN 4'b0011
               `else
                  `ifdef AR_BURST_2
                     `define AR_BURST_EN 4'b0010
                  `else
                     `ifdef AR_BURST_1
                        `define AR_BURST_EN 4'b0001
                     `endif
                  `endif
               `endif
            `endif
         `endif
      `endif
   `endif
`endif 

`define COL_WIDTH 9
`define COL_WIDTH_LT_10

`define TRAS    4'b1000
`define TRC     4'b1100
`define TRCD    3'b011
`define TRP     2'b11
`define TWTR    2'b10
`define TXP     2'b10

`define TMRD    2'b10
`define TRFC    5'b01111
`define TSRC    3'b010
`define TSRR    2'b11
`define TWR     2'b10
`define TCKE    3'b100
`define TXSR    6'b011011

`define TREFI   11'b10000010000
`define TREFI_WIDTH 11

`define MRS_REG 16'h0032

`define EMRS_REG 16'h0000

`define TRP_WIDTH 2
`define TRFC_WIDTH 5
`define TMRD_WIDTH 2
`define TRFCSR_WIDTH 5
`define TXP_WIDTH 2
`define TCKE_WIDTH 3
`define TXSR_WIDTH 6

`define TRCD_WIDTH 3
`define TRAS_WIDTH 4
`define TRC_WIDTH 4
`define TWTR_WIDTH 2
`define TWR_WIDTH 2
`define TSRR_WIDTH 2
`define TSRC_WIDTH 3

`define CL_WIDTH 3
`define RL_WIDTH 3
`define BANK_WIDTH 2

`ifdef DELAY_0  // values 0,1,2,10,50
   `define DELAY 0
`else
   `ifdef DELAY_1
      `define DELAY 1
   `else
      `ifdef DELAY_2
         `define DELAY 2
      `else
         `ifdef DELAY_10
            `define DELAY 10
         `else
            `ifdef DELAY_50
               `define DELAY 50
            `endif
         `endif
      `endif
   `endif
`endif

`define DATA_WIDTH      8
`define DSIZE           16
`define USER_DM         2
`define DMSIZE          1
`define DQS_WIDTH       1
`define DQS_WIDTH_1

`define BRST_CNT 4'hF
`define BRST_CNT_WIDTH 4
`define D200us 4'h7
